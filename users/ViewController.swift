//
//  ViewController.swift
//  users
//
//  Created by David Trivian S on 11/28/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import UIKit
import NetworkExtension

class ViewController: UIViewController {
    var stringLastSSID = ""
    @IBOutlet weak var ssidTextfield: UITextField!
    
    @IBOutlet weak var passTextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
    }
    @IBAction func actionRemoveConfiguration(_ sender: UIButton) {
        NEHotspotConfigurationManager.shared.removeConfiguration(forSSID: stringLastSSID)
        NEHotspotConfigurationManager.shared.removeConfiguration(forHS20DomainName: stringLastSSID)
    }
    @IBAction func actionConnect(_ sender: UIButton) {
        stringLastSSID =  ssidTextfield.text!
        let hotspotConfig = NEHotspotConfiguration(ssid:ssidTextfield.text!)
        hotspotConfig.joinOnce =  false
        let options: [String: NSObject] = [kNEHotspotHelperOptionDisplayName : "Try here"as NSObject]
        let queue: DispatchQueue = DispatchQueue(label: "com.wiforgo.users", attributes: DispatchQueue.Attributes.concurrent)
        
        
        NEHotspotHelper.register(options: options, queue: queue) { (cmd: NEHotspotHelperCommand) in
            print("Received command: \(cmd.commandType.rawValue)")
            NSLog("Received command: \(cmd.commandType.rawValue)")
        }
        NEHotspotConfigurationManager.shared.apply(hotspotConfig) {[unowned self] (error) in
            
            if let error = error {
                print("cannot connect \(error)")
            }
            else {
                print("success connect")
                self.showSuccess()
               
            }
        }

       
    }
    private func showError(error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Darn", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    private func showSuccess() {
        let alert = UIAlertController(title: "", message: "Open Settings", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { _ in
            let url = URL(string: "App-Prefs:root=WIFI") //for WIFI setting app
            let app = UIApplication.shared
            app.open(url!, options: [:], completionHandler: { (complete) in
                if complete {
                    
                }
            })
            
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

